<?php
class Login_model extends CI_Model{

    function __construct(){
        $this->load->database();
    }

    /*
     * Devuelve el rol del usuario logado
     */
    function login($data){
        $sql = 'SELECT COUNT(*) as campo
                    FROM usuario
                    WHERE login = "' . $data['login'] . '"
                    AND password = SHA1("' . $data['password'] . '")
                    AND activo = 1';
        $query = $this->db->query($sql);
        $aux = $query->result();
        return (bool)$aux[0]->campo;
    }

    function changePass($data) {
        $sql = 'UPDATE usuario SET password = SHA1("' . $data['password'] . '") WHERE login = "' . strtolower($data['login']) . '"';
        $query = $this->db->query($sql);
        return $query;
    }

    function getUsers(){
        $sql = 'SELECT * FROM usuario';
        $query = $this->db->query($sql);
        $res = array();
        $result = array();
        foreach ($query->result() as $row) {
            $res['id'] = $row->id;
            $res['nombre'] = $row->nombre;
            $res['email'] = $row->email;
            $res['password'] = $row->password;
            $res['login'] = $row->login;
            $res['activo'] = $row->activo;
            $result[] = $res;
        }

        return $query->result();
    }

    function getUserData($user, $campo){
        $sql = 'SELECT id, nombre FROM usuario WHERE login = \'' . $user . '\'';
        $query = $this->db->query($sql);
        $aux = $query->result();
        return $aux[0]->$campo;
    }
}
