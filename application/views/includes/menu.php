<div class="panel-heading">
    <div class="row">
        <div class="col-xs-3">
            <img width="60" src="<?php echo base_url() . 'assets/images/logo.jpg' ?>">
        </div>
        <div class="col-xs-9">
            <h3>Hola<?php echo ($this->session->userdata('logged')) ? ' ' . $this->session->userdata('username') . '!' : ', bienvenido!'?></h3>
        </div>
    </div>
    <div class="row">
    <?php
    if ($this->session->userdata('logged')): ?>
        <div class="col-xs-2">
            <?php echo anchor("index.php/home/index", "Lista pacientes", array("title" => "Lista pacientes", "class" => "btn btn-success", "style" => "float: left; top: 20px; position: relative;"));?>
        </div>
        <div class="col-xs-2">
            <?php echo anchor("index.php/home/adminPacientes", "Administrar pacientes", array("title" => "Administrar pacientes", "class" => "btn btn-warning", "style" => "float: left; top: 20px; position: relative;"));?>
        </div>
        <div class="col-xs-2">
            <?php echo anchor("index.php/home/config", "Configuración", array("title" => "Configuración", "class" => "btn btn-info", "style" => "float: left; top: 20px; position: relative;"));?>
        </div>
        <div class="col-xs-1">
            <?php echo anchor("index.php/home/logout", "Logout", array("title" => "Logout", "class" => "btn btn-danger", "style" => "float: left; top: 20px; position: relative;"));?>
        </div>
        <?php
        if ($this->session->userdata('login') == 'admin'): ?>
            </div>
            <div class="row" style="margin-top: 10px;">
                <div class="col-xs-2">
                    <?php echo anchor("index.php/home/admin/usuario", "Admin usuarios", array("title" => "Admin usuarios", "class" => "btn btn-primary", "style" => "float: left; top: 20px; position: relative;"));?>
                </div>
                <div class="col-xs-2">
                    <?php echo anchor("index.php/home/admin/paciente", "Admin pacientes", array("title" => "Admin pacientes", "class" => "btn btn-primary", "style" => "float: left; top: 20px; position: relative;"));?>
                </div>
                <div class="col-xs-2">
                    <?php echo anchor("index.php/home/admin/datosPaciente", "Admin datos pacientes", array("title" => "Admin datos pacientes", "class" => "btn btn-primary", "style" => "float: left; top: 20px; position: relative;"));?>
                </div>
                <div class="col-xs-2">
                    <?php echo anchor("index.php/home/admin/ficha", "Admin fichas", array("title" => "Admin fichas", "class" => "btn btn-primary", "style" => "float: left; top: 20px; position: relative;"));?>
                </div>
            </div>
        <?php endif; ?>
    <?php endif;?>
    </div>
</div>