<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<?php $this->load->view('includes/topbody.php');?>
<?php $this->load->view('fichas/headFicha.php');?>

<?php

if (count($datosFicha) == 0):
    echo anchor(
        "index.php/home/newDatosFicha/" . $paciente[0]->id,
        "Añadir primera consulta",
        array(
            "title" => "Añadir primera consulta",
            "class" => "btn btn-success btn-large"
        )
    );

elseif (count($datosFicha) > 0 && count($datosSubficha) == 0):
    echo anchor(
        "index.php/home/newDatosSubficha/" . $paciente[0]->id,
        "Añadir nueva ficha",
        array(
            "title" => "Añadir primera consulta",
            "class" => "btn btn-success btn-large"
        )
    );
endif; ?>

<table id="fichas" class="table table-striped table-bordered" cellspacing="0" width="100%">
    <thead>
    <tr style="cursor: pointer">
        <th>Fecha</th>
        <th>Primera consulta</th>
        <th>Ver</th>
        <th>Editar</th>
    </tr>
    </thead>
    <tbody>
    <?php $i = 0;
    if(count($datosFicha) > 0):
        foreach($datosFicha as $f): $i++; ?>
            <tr>
                <td style="font-size:1.2em"><b><?php echo mostrarFecha($f->cdate);?></b></td>
                <td>Si</td>
                <td><?php echo anchor(base_url() . 'index.php/home/datosFicha/' . $f->idPaciente, 'Ficha primera consulta', array("title" => 'Primera consulta', "class" => "btn btn-success"));?></td>
                <td><?php echo anchor(base_url() . 'index.php/home/editFicha/' . $f->idPaciente, 'Modificar', array("title" => 'Primera consulta', "class" => "btn btn-success"));?></td>
            </tr>
            <?php
        endforeach;
    endif;
    if(count($subfichas) > 0):
        foreach($subfichas as $f): $i++;?>
            <tr>
                <td style="font-size:1.2em"><b><?php echo mostrarFecha($f->cdate);?></b></td>
                <td>No</td>
                <td><?php echo anchor(base_url() . 'index.php/home/datosSubficha/' . $f->id, 'Ficha ' . mostrarFecha($f->cdate), array("title" => 'Ficha', "class" => "btn btn-info"));?></td>
                <td><?php echo anchor(base_url() . 'index.php/home/editSubficha/' . $f->id, 'Modificar', array("title" => 'Ficha', "class" => "btn btn-info"));?></td>
            </tr>
            <?php
        endforeach;
    endif;
    ?>
    </tbody>
    <tfoot>
    <tr style="cursor: pointer">
        <th>Fecha</th>
        <th>Primera consulta</th>
        <th>Ver</th>
        <th>Editar</th>
    </tr>
    </tfoot>
</table>
<script>
$(document).ready(function() {
    $('#fichas').dataTable( {
        "order": [[ 1, "desc" ]]
    } );
} );
</script>

<?php $this->load->view('includes/subbody.php');?>
