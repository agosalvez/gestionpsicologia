<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<?php $this->load->view('includes/topbody.php');?>
<?php $this->load->view('fichas/headFicha.php');?>
<?php $this->load->view('fichas/navFichas.php');?>
    <div class="col-xs-11">
        <h4>Fecha de la ficha: <?php echo mostrarFecha($subfichas[0]->cdate); ?></h4>
    </div>
</div>

<div class="row">
    <div class="col-xs-12">
        <div class="row" style="text-align: center;">
            <div class="panel panel-success">
                <div class="panel-heading" style="font-size: 1.4em">
                    Detalles
                </div>
                <div class="panel-body">
                    <?php echo $subfichas[0]->detalles ?>
                </div>
                <div class="panel-heading" style="font-size: 1.4em">
                    Anotaciones
                </div>
                <div class="panel-body">
                    <?php echo $subfichas[0]->anotaciones ?>
                </div>
                <div class="panel-heading" style="font-size: 1.4em">
                    Tratamientos
                </div>
                <div class="panel-body">
                    <?php echo $subfichas[0]->tratamientos ?>
                </div>
            </div>
        </div>
    </div>
</div>
<br>

<?php $this->load->view('includes/subbody.php');?>
