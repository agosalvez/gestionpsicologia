<table id="pacientes" class="table table-striped table-bordered" cellspacing="0" width="100%">
    <thead>
    <tr style="cursor: pointer">
        <th>Nombre</th>
        <th>Población</th>
        <th>Teléfono</th>
        <th>Historial clínico</th>
    </tr>
    </thead>
    <tbody>
    <?php $i = 0;
    if(isset($pacientes)):
        foreach($pacientes as $f): $i++; ?>
            <tr>
                <td style="font-size:1.2em"><b><?php echo $f->nombre . ' ' . $f->apellidos;?></b></td>
                <td><?php echo $f->poblacion;?></td>
                <td><?php echo $f->telefono1;?></td>
                <td><?php echo anchor(base_url() . 'index.php/home/ficha/' . $f->id, 'Ver ficha', array("title" => 'Ver ficha', "class" => "btn btn-success"));?></td>
            </tr>
            <?php
        endforeach;
    endif;
    ?>
    </tbody>
    <tfoot>
    <tr style="cursor: pointer">
        <th>Nombre</th>
        <th>Población</th>
        <th>Teléfono</th>
        <th>Historial clínico</th>
    </tr>
    </tfoot>
</table>
<script>
$(document).ready(function() {
    $('#pacientes').dataTable( {
        "order": [[ 1, "desc" ]]
    } );
} );
</script>
