<!DOCTYPE html>
<html>
<head>
    <title>Maria Botella Psicología</title>
	<meta charset="utf-8">
	<meta http-equiv="content-type" content="text/html" charset="utf-8">

<!--	<link rel="icon" href="--><?php //echo base_url() . 'assets/images/favicon.png'?><!--"/>-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'assets/css/bootstrap.min.css'?>">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'assets/css/bootstrap-theme.min.css'?>">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'assets/css/style.min.css'?>">

	<script type="text/javascript" src="<?php echo base_url() . 'assets/js/jquery-3.1.1.min.js'?>"></script>
	<script type="text/javascript" src="<?php echo base_url() . 'assets/js/bootstrap.js'?>"></script>
	<script type="text/javascript" src="<?php echo base_url() . 'assets/js/scripts.min.js'?>"></script>
	<script type="text/javascript" src="https://code.jquery.com/jquery-1.12.4.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>

    <!-- Grocery crud -->
    <?php
        if (isset($css_files)) {
            foreach($css_files as $filecss): ?>
                <link type="text/css" rel="stylesheet" href="<?php echo $filecss; ?>" />
            <?php endforeach;
        } elseif (isset($output->css_files)) { // para cuando pasamos los datos del grocery mediante un array indexado con mas variables
            foreach($output->css_files as $filecss): ?>
                <link type="text/css" rel="stylesheet" href="<?php echo $filecss; ?>" />
            <?php endforeach;
        }
        if (isset($js_files)) {
            foreach($js_files as $filejs): ?>
                <script src="<?php echo $filejs; ?>"></script>
            <?php endforeach;
        } elseif (isset($output->js_files)) { // para cuando pasamos los datos del grocery mediante un array indexado con mas variables
            foreach($output->js_files as $filejs): ?>
                <script src="<?php echo $filejs; ?>"></script>
            <?php endforeach;
        }
    ?>
</head>
