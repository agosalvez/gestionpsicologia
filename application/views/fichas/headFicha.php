<br>
<div class="row">
    <div class="panel panel-success">
        <div class="panel-heading" style="font-size: 1.4em; text-align: center;">
            <?php echo $paciente[0]->nombre . ' ' . $paciente[0]->apellidos; ?>
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-xs-4">
                    <div class="row">
                        <div class="col-xs-5">
                            <label>Nombre</label>
                        </div>
                        <div class="col-xs-6">
                            <?php echo ucfirst($paciente[0]->nombre) . ' ' . ucfirst($paciente[0]->apellidos); ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-5">
                            <label>Fecha nacimiento</label>
                        </div>
                        <div class="col-xs-6">
                            <?php echo ($paciente[0]->fechaNac == '0000-00-00') ? $paciente[0]->edad . ' años' : mostrarFecha($paciente[0]->fechaNac) . ' (' . calcularEdad($paciente[0]->fechaNac) . ' años)';?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-5">
                            <label>Estado civil</label>
                        </div>
                        <div class="col-xs-6">
                            <?php echo ucfirst($paciente[0]->estadoCivil); ?>
                        </div>
                    </div>
                </div>
                <div class="col-xs-4">
                    <div class="row">
                        <div class="col-xs-5">
                            <label>Situación laboral</label>
                        </div>
                        <div class="col-xs-6">
                            <?php echo ucfirst($paciente[0]->sitLabAct); ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-5">
                            <label>Población</label>
                        </div>
                        <div class="col-xs-6">
                            <?php echo ucfirst($paciente[0]->poblacion); ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-5">
                            <label>Fecha de alta</label>
                        </div>
                        <div class="col-xs-6">
                            <?php echo mostrarFecha($paciente[0]->cdata); ?>
                        </div>
                    </div>
                </div>
                <div class="col-xs-4">
                    <div class="row">
                        <div class="col-xs-4">
                            <label>Teléfono</label>
                        </div>
                        <div class="col-xs-4">
                            <?php echo number_format($paciente[0]->telefono1, 0, '', ' '); ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-4">
                            <label>Teléfono 2</label>
                        </div>
                        <div class="col-xs-4">
                            <?php echo number_format($paciente[0]->telefono2, 0, '', ' '); ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-4">
                            <label>Teléfono 3</label>
                        </div>
                        <div class="col-xs-4">
                            <?php echo number_format($paciente[0]->telefono3, 0, '', ' '); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<br>