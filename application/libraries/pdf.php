<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class PDF extends FPDF
{
    function __construct($numFactura = 0, $fechaFactura = '', $datosEmpresa = '')
    {
        parent::__construct();
        $this->numFactura = $numFactura;
        $this->fechaFactura = $fechaFactura;
        $this->nombreE = $datosEmpresa['nombre'];
        $this->direccionE = $datosEmpresa['direccion'];
        $this->cpE = $datosEmpresa['cpostal'];
        $this->localidadE = $datosEmpresa['localidad'];
        $this->provinciaE = $datosEmpresa['provincia'];
        $this->paisE = $datosEmpresa['pais'];
        $this->piePagina = $datosEmpresa['piePagina'];
    }

    // Cabecera de página
    function Header()
    {
        $setXEmpresa = 150;
        $this->SetFont('Arial','B',16);
        $this->SetXY(20, 15);
        $this->Cell(50,10,utf8_decode('Factura nº    ' . $this->numFactura),0,1,'L');

        $this->SetFont('Arial','',13);
        $this->SetXY(20, 22);
        $this->Cell(50,10,utf8_decode('Fecha                ' . $this->fechaFactura),0,0,'L');

        // Logo
        $this->Image(base_url().'/assets/images/ekoe.png',115,18,33);

        // Arial bold 10
        $this->SetFont('Arial','B',10);

        $this->SetXY($setXEmpresa,10);
        $this->Cell(30,10,utf8_decode($this->nombreE),0,0,'L');

        $this->SetXY($setXEmpresa,15);
        $this->Cell(30,10,utf8_decode($this->direccionE),0,0,'L');

        $this->SetXY($setXEmpresa,20);
        $this->Cell(30,10,utf8_decode($this->cpE . ' ' . $this->localidadE . ' (' . $this->provinciaE . ')'),0,0,'L');

        $this->SetXY($setXEmpresa,25);
        $this->Cell(30,10,utf8_decode($this->paisE),0,0,'L');

        $this->Ln(5);
    }

    // Pie de página
    function Footer()
    {
        $alturaFooter = -45;
        $ejeX = 20;

        // Posición: a 1,5 cm del final
        $this->SetXY($ejeX, $alturaFooter);
        // Arial italic 8
        $this->SetFont('Arial','B',9);
        // Número de página
        $this->Cell(170,25,'',1,0,'L');

        $this->SetXY($ejeX, $alturaFooter-=7);
        $this->Cell(170,20,'Observaciones',0,0,'L');

        $this->SetXY($ejeX, $alturaFooter+=25);
        $this->Cell(170,20,utf8_decode($this->piePagina),0,0,'L');
    }
}