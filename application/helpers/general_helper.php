<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('parsearProductosCantidades'))
{
    function mostrarFecha($cad) {
        return date('d-m-Y', strtotime($cad));
    }

    function calcularEdad($fecha) {
        $tiempo = strtotime($fecha);
        $ahora = time();
        $edad = ($ahora-$tiempo)/(60*60*24*365.25);
        $edad = floor($edad);
        return $edad;
    }
}