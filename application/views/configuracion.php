<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<?php $this->load->view('includes/topbody.php');?>
<?php if (isset($msg) && isset($color)) :?>
    <div class="alert alert-<?php echo $color ?>">
        <strong><?php echo $head ?></strong> <?php echo $msg ?>
    </div>
<?php endif;?>
<form class="" method="POST" action="<?php echo base_url() . 'index.php/home/passChange'?>">
    <div class="form-group">
        <label for="pass1">Nueva contraseña *</label>
        <input type="password" minlength="5" required class="form-control" id="pass1" name="pass">
    </div>
    <div class="form-group">
        <label for="pass2">Repite contraseña *</label>
        <input type="password" minlength="5" required class="form-control" id="pass2">
    </div>
    <button type="submit" id="btnChgPass" class="btn btn-default">Cambiar contraseña</button>
</form>
<div id="msg-container"></div>
<?php $this->load->view('includes/subbody.php');?>