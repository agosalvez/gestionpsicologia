<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
 * Controlador principal de la aplicación
 */
class home extends CI_Controller {

    /*
     * Constructor por defecto
     */
    function __construct() {
        parent::__construct();
    }
    /*
     * Método principal controlador de usuarios
     * Lista de videos
     */
	public function index() {
        if ($this->session->userdata('logged')) {
            // Pasamos los datos del listado para que los cargue de primera instancia
            $data = array(
                'pacientes' => $this->paciente_model->getPacientes(null, 'id, nombre, poblacion, telefono1')
            );
            $this->load->view('index', $data);
        } else {
            $this->load->view('login');
        }
	}
	/*
	 * Gestiona el login de la aplicación
	 */
	public function login() {
        if ($this->session->userdata('logged')) {
            $this->index();
        } else {
            $data = array(
              'login' =>   strtolower($this->input->post('login')),
                'password' => $this->input->post('password')
            );

            if ($this->login_model->login($data)) {
                $this->session->set_userdata('logged', true);
                $this->session->set_userdata('login', $data['login']);
                $this->session->set_userdata('idUsuario', $this->login_model->getUserData($data['login'], 'id'));
                $this->session->set_userdata('username', $this->login_model->getUserData($data['login'], 'nombre'));
                $this->index();
            } else {
                $this->logout();
            }

        }
    }
    /*
     * Logout de la aplicación
     */
    public function logout() {
        $this->session->unset_userdata('user');
        $this->session->unset_userdata('logged');
        $this->load->view('login');
    }

    /*
     * Proporciona la vista configuracion
     */
    public function config($data = null) {
        $this->load->view('configuracion', $data);
    }
    /*
     * Recibe el formuario para cambio de contraseña
     */
    public function passChange() {
        if ($this->session->userdata('logged')) {
            $data = array(
                'password' => $this->input->post('pass'),
                'login' => $this->session->userdata('login')
            );

            if ($this->login_model->changePass($data)) {
                $data = array(
                    'head' => 'Correcto!',
                    'msg' => 'Contraseña cambiada correctamente',
                    'color' => 'success'
                );
            } else {
                $data = array(
                    'head' => 'Error!',
                    'msg' => 'Error al cambiar la contarseña. Por favor, no introduzca caracteres raros ni acentos.',
                    'color' => 'danger'
                );
            }
            $this->config($data);
        }
    }

    /*
     * Configuración del CRUD de pacientes
     */
    public function adminPacientes() {
        $table = 'paciente';
        $crud = new grocery_CRUD();
        $crud->set_table($table);
        $crud->set_theme('flexigrid');
        $crud->set_subject(strtolower($table));

        $crud->display_as('sitLabAct', 'Situación laboral actual');
        $crud->display_as('cdate', 'Alta de paciente');

        $crud->unset_columns(array('idUsuario'));

        $crud->change_field_type('idUsuario','invisible');
        $crud->change_field_type('cdate','invisible');
        $crud->change_field_type('activo','invisible');

        $crud->callback_before_insert(array($this,'callback_before_insert_paciente'));

        $output = $crud->render();
        $this->load->view('admin/admin_' . $table, $output);
    }

    /*
     * Función callback antes de insertar
     * En esta función seteo el idUsuario y la cdate, ya que se oculta en el formulario normal.
     */
    function callback_before_insert_paciente($post_array)
    {
        $post_array['cdate'] = date('Y-m-d H:i:s');
        $post_array['idUsuario'] = $this->session->userdata('idUsuario');
        return $post_array;
    }


    /*
     * Gestiona las vista de CRUD para el admin (genérico)
     * $table: Nombre de la tabla a realizar el CRUD
     */
     public function admin($table) {
        $crud = new grocery_CRUD();
        $crud->set_table($table);
        $crud->set_theme('flexigrid');
        $crud->set_subject(strtolower($table));

        $output = $crud->render();
        $this->load->view('admin/admin_' . $table, $output);
    }

    /*
     * Mostramos el listado de todas las fichas del paciente
     */
    public function ficha($idPaciente) {
        $data = array(
            'paciente' => $this->paciente_model->getPacientes($idPaciente),
          'datosFicha' => $this->paciente_model->getDatosPaciente($idPaciente, 'cdate, idPaciente'),
            'subfichas' => $this->paciente_model->getSubfichas($idPaciente, 'cdate, id')
        );
        $this->load->view('fichas/fichas', $data);
    }

    /*
     * Mostramos la vista de ficha primera visita
     */
    public function datosFicha($idPaciente) {
        $data = array(
            'paciente' => $this->paciente_model->getPacientes($idPaciente),
            'datosFicha' => $this->paciente_model->getDatosPaciente($idPaciente)
        );
        $this->load->view('fichas/datosFicha', $data);
    }

    /*
     * Mostramos la vista de subficha
     */
    public function datosSubficha($idSubficha) {
        $data = array(
            'paciente' => $this->paciente_model->getPacientes($idPaciente),
            'subfichas' => $this->paciente_model->getSubfichas(null, null, $idSubficha)
        );
        $this->load->view('fichas/datosSubficha', $data);
    }

    /*
     * CRUD personalizado para editar una ficha primera consulta
     */
    public function editFicha($idPaciente) {
        $table = 'datosPaciente';
        $crud = new grocery_CRUD();
        $crud->set_table($table);
        $crud->set_theme('flexigrid');
        $crud->set_subject(strtolower($table));

        $crud->where('idPaciente', $idPaciente);
        $crud->unset_add();

        $crud->display_as('tratActual', 'Tratamiento actual');
        $crud->display_as('tratAnterior', 'Tratamiento anterior');
        $crud->display_as('infoEnfermedad', 'Información de la enfermedad');
        $crud->display_as('relSociales', 'Relaciones sociales');
        $crud->display_as('dream', 'Sueño');
        $crud->display_as('cdate', 'Fecha');

        $crud->field_type('idPaciente', 'invisible');
        $crud->unset_columns('idPaciente');

        $output = $crud->render();
        $this->load->view('admin/admin_datosFicha', $output);
    }

    /*
     * CRUD personalizado apra editar una subficha
     */
    public function editSubficha($idSubficha) {
        $table = 'ficha';
        $crud = new grocery_CRUD();
        $crud->set_table($table);
        $crud->set_theme('flexigrid');
        $crud->set_subject(strtolower($table));

        $crud->where('id', $idSubficha);
        $crud->unset_add();

        $crud->display_as('cdate', 'Fecha');

        $crud->field_type('idPaciente', 'invisible');
        $crud->unset_columns('idPaciente');

        $output = $crud->render();
        $this->load->view('admin/admin_datosSubficha', $output);
    }

    /*
     * Función que añade la primera tupla de datosFicha
     */
    public function newDatosFicha($idPaciente) {
        $table = 'datosPaciente';
        $crud = new grocery_CRUD();
        $crud->set_table($table);
        $crud->set_theme('flexigrid');
        $crud->set_subject(strtolower($table));

        $crud->where('idPaciente', $idPaciente);

        $crud->display_as('tratActual', 'Tratamiento actual');
        $crud->display_as('tratAnterior', 'Tratamiento anterior');
        $crud->display_as('infoEnfermedad', 'Información de la enfermedad');
        $crud->display_as('relSociales', 'Relaciones sociales');
        $crud->display_as('situacionLaboral', 'Situación laboral');
        $crud->display_as('dream', 'Sueño');
        $crud->display_as('cdate', 'Fecha');

        $crud->required_fields('cdate');

        $crud->field_type('idPaciente', 'invisible');
        $crud->unset_columns('idPaciente');
        $this->idPaciente = $idPaciente;

        $crud->callback_before_insert(array($this,'callback_before_insert_datosPaciente'));

        $output = $crud->render();
        $this->load->view('fichas/newDatosFicha', $output);
    }

    /*
     * Función callback antes de insertar
     * En esta función seteo el idPaciente, ya que se oculta en el formulario normal.
     */
    public function callback_before_insert_datosPaciente($post_array)
    {
        $post_array['idPaciente'] = $this->idPaciente;
        return $post_array;
    }

    /*
     * Función que añade subfichas a un paciente
     */
    public function newDatosSubficha($idPaciente) {
        $table = 'ficha';
        $crud = new grocery_CRUD();
        $crud->set_table($table);
        $crud->set_theme('flexigrid');
        $crud->set_subject(strtolower($table));

        $crud->required_fields('cdate');

        $crud->where('idPaciente', $idPaciente);

        $crud->display_as('cdate', 'Fecha');

        $crud->field_type('idPaciente', 'invisible');
        $crud->unset_columns('idPaciente');
        $this->idPaciente = $idPaciente;

        $crud->callback_before_insert(array($this,'callback_before_insert_datosSubficha'));

        $output = $crud->render();
        $this->load->view('fichas/newDatosSubficha', $output);
    }

    /*
     * Función callback antes de insertar
     * En esta función seteo el idPaciente, ya que se oculta en el formulario normal.
     */
    public function callback_before_insert_datosSubficha($post_array)
    {
        $post_array['idPaciente'] = $this->idPaciente;
        return $post_array;
    }

}

/* End of file home.php */
/* Location: ./application/controllers/home.php */
