<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<?php $this->load->view('includes/header.php');?>
<body>
<header class="container">
    <?php $this->load->view('includes/menu')?>
</header>
<main class="container" >
	<div class="panel-group panel-primary col-xs-12">
		<div class="panel-body">
			<div class="row">
                <form method="POST" action="<?php echo base_url() . 'index.php/home/login'?>">
                    <div class="form-group">
                        <label for="login">Login</label>
                        <input id="login" name="login" class="form-control" type="text" placeholder="Login">
                    </div>
                    <div class="form-group">
                        <label for="password">Password</label>
                        <input id="password" name="password" class="form-control" type="password" placeholder="Password">
                    </div>
                    <button type="submit" class="btn btn-default">Iniciar sesión</button>
                </form>
			</div>
		</div>
	</div>
</main>
</body>
<?php $this->load->view('includes/footer.php');?>
</html>
