<?php
class Paciente_model extends CI_Model{

    function __construct(){
        $this->load->database();
    }

    /*
     * Devuelve los pacientes almacenados en bbdd
     */
    function getPacientes($idPaciente = null, $campo = null){
        $campos = ($campo != null) ? $campo : '*';
        $where = ($idPaciente != null) ? ' AND id = ' . $idPaciente . ' ' : '';
        $sql = 'SELECT ' . $campos . '
                FROM paciente
                WHERE 1
                ' . $where . ' 
                ORDER BY id DESC';
        echo ($this->session->userdata('login') == 'admin') ? $sql .'<br />': '';
        $query = $this->db->query($sql);
        $res = array();
        $result = array();
        foreach ($query->result() as $row) {
            $res['id'] = $row->id;
            $res['idUsuario'] = $row->idUsuario;
            $res['nombre'] = $row->nombre;
            $res['apellidos'] = $row->apellidos;
            $res['telefono'] = $row->telefono;
            $res['email'] = $row->email;
            $res['codpostal'] = $row->codpostal;
            $res['localidad'] = $row->localidad;
            $res['provincia'] = $row->provincia;
            $res['activo'] = $row->activo;
            $res['cdate'] = $row->cdate;
            $result[] = $res;
        }

        return $query->result();
    }

    /*
     * Devuelve los datos de un paciente pasado por parametro
     */
    function getDatosPaciente($idPaciente = null, $campo = null){
        $paciente = ($idPaciente != null) ? ' AND idPaciente = ' . $idPaciente . ' ' : '';
        $campos = ($campo != null) ? $campo : '*';
        $sql = 'SELECT ' . $campos . '
                FROM datosPaciente 
                WHERE 1
                ' . $paciente . '
                ORDER BY cdate DESC';
        echo ($this->session->userdata('login') == 'admin') ? $sql.'<br />' : '';
        $query = $this->db->query($sql);
        $res = array();
        $result = array();

        foreach ($query->result() as $row) {
            $res['idPaciente'] = $row->idPaciente;
            $res['motivo'] = $row->motivo;
            $res['diagnostico'] = $row->diagnostico;
            $res['tratActual'] = $row->tratActual;
            $res['tratAnterior'] = $row->tratAnterior;
            $res['infoEnfermedad'] = $row->infoEnfermedad;
            $res['dream'] = $row->dream;
            $res['alimentacion'] = $row->alimentacion;
            $res['tabaco'] = $row->tabaco;
            $res['alcohol'] = $row->alcohol;
            $res['drogas'] = $row->drogas;
            $res['deporte'] = $row->deporte;
            $res['pasatiempos'] = $row->pasatiempos;
            $res['emociones'] = $row->emociones;
            $res['relSociales'] = $row->relSociales;
            $res['pareja'] = $row->pareja;
            $res['hijos'] = $row->hijos;
            $res['familia'] = $row->familia;
            $res['detalles'] = $row->detalles;
            $res['anotaciones'] = $row->anotaciones;
            $res['tratamientos'] = $row->tratamientos;
            $res['cdate'] = $row->cdate;
            $result[] = $res;
        }

        return $query->result();
    }

    /*
     * Devuelve las fichas de un paciente pasado por parametro
     */
    function getSubfichas($idPaciente = null, $campo = null, $idSubficha = null){
        $paciente = ($idPaciente != null) ? ' AND idPaciente = ' . $idPaciente . ' ' : '';
        $campos = ($campo != null) ? $campo : '*';
        $where = ($idSubficha != null) ? ' AND id = ' . $idSubficha : '';
        $sql = 'SELECT ' . $campos . '
                FROM ficha
                WHERE 1
                ' . $where . '
                ' . $paciente . '
                ORDER BY cdate DESC';
        echo ($this->session->userdata('login') == 'admin') ? $sql .'<br />': '';
        $query = $this->db->query($sql);
        $res = array();
        $result = array();
        foreach ($query->result() as $row) {
            $res['idPaciente'] = $row->idPaciente;
            $res['detalles'] = $row->detalles;
            $res['anotaciones'] = $row->anotaciones;
            $res['tratamientos'] = $row->tratamientos;
            $res['cdate'] = $row->cdate;
            $result[] = $res;
        }
        return $query->result();
    }
}
