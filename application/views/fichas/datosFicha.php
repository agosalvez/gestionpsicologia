<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<?php $this->load->view('includes/topbody.php');?>
<?php $this->load->view('fichas/headFicha.php');?>
<?php $this->load->view('fichas/navFichas.php');?>
    <div class="col-xs-11">
        <h4>Fecha de la ficha: <?php echo mostrarFecha($datosFicha[0]->cdate); ?></h4>
    </div>
</div>
<div class="row">
    <div class="col-xs-12">
        <div class="row" style="text-align: center;">
            <div class="panel panel-success">
                <div class="panel-heading" style="font-size: 1.4em">
                    Motivo
                </div>
                <div class="panel-body">
                    <?php echo $datosFicha[0]->motivo ?>
                </div>
                <div class="panel-heading" style="font-size: 1.4em">
                    Diagnóstico
                </div>
                <div class="panel-body">
                    <?php echo $datosFicha[0]->diagnostico ?>
                </div>
                <div class="panel-heading" style="font-size: 1.4em">
                    Tratamiento actual
                </div>
                <div class="panel-body">
                    <?php echo $datosFicha[0]->tratActual ?>
                </div>
                <div class="panel-heading" style="font-size: 1.4em">
                    Tratamiento anterior
                </div>
                <div class="panel-body">
                    <?php echo $datosFicha[0]->tratAnterior ?>
                </div>
                <div class="panel-heading" style="font-size: 1.4em">
                    Información de la enfermedad
                </div>
                <div class="panel-body">
                    <?php echo $datosFicha[0]->infoEnfermedad ?>
                </div>
                <div class="panel-heading" style="font-size: 1.4em">
                    Situación laboral
                </div>
                <div class="panel-body">
                    <?php echo $datosFicha[0]->situacionLaboral ?>
                </div>
                <div class="panel-heading" style="font-size: 1.4em">
                    Sueño
                </div>
                <div class="panel-body">
                    <?php echo $datosFicha[0]->dream ?>
                </div>
                <div class="panel-heading" style="font-size: 1.4em">
                    Alimentación
                </div>
                <div class="panel-body">
                    <?php echo $datosFicha[0]->alimentacion ?>
                </div>
                <div class="panel-heading" style="font-size: 1.4em">
                    Tabaco
                </div>
                <div class="panel-body">
                    <?php echo $datosFicha[0]->tabaco ?>
                </div>
                <div class="panel-heading" style="font-size: 1.4em">
                    Alcohol
                </div>
                <div class="panel-body">
                    <?php echo $datosFicha[0]->alcohol ?>
                </div>
                <div class="panel-heading" style="font-size: 1.4em">
                    Drogas
                </div>
                <div class="panel-body">
                    <?php echo $datosFicha[0]->drogas ?>
                </div>
                <div class="panel-heading" style="font-size: 1.4em">
                    Deporte / Ejercicio físico
                </div>
                <div class="panel-body">
                    <?php echo $datosFicha[0]->deporte ?>
                </div>
                <div class="panel-heading" style="font-size: 1.4em">
                    Pasatiempos / Ocio
                </div>
                <div class="panel-body">
                    <?php echo $datosFicha[0]->pasatiempos ?>
                </div>
                <div class="panel-heading" style="font-size: 1.4em">
                    Emociones
                </div>
                <div class="panel-body">
                    <?php echo $datosFicha[0]->emociones ?>
                </div>
                <div class="panel-heading" style="font-size: 1.4em">
                    Relaciones sociales
                </div>
                <div class="panel-body">
                    <?php echo $datosFicha[0]->relSociales ?>
                </div>
                <div class="panel-heading" style="font-size: 1.4em">
                    Pareja
                </div>
                <div class="panel-body">
                    <?php echo $datosFicha[0]->pareja ?>
                </div>
                <div class="panel-heading" style="font-size: 1.4em">
                    Hijos
                </div>
                <div class="panel-body">
                    <?php echo $datosFicha[0]->hijos ?>
                </div>
                <div class="panel-heading" style="font-size: 1.4em">
                    Familia
                </div>
                <div class="panel-body">
                    <?php echo $datosFicha[0]->familia ?>
                </div>
                <div class="panel-heading" style="font-size: 1.4em">
                    Detalles
                </div>
                <div class="panel-body">
                    <?php echo $datosFicha[0]->detalles ?>
                </div>
                <div class="panel-heading" style="font-size: 1.4em">
                    Anotaciones
                </div>
                <div class="panel-body">
                    <?php echo $datosFicha[0]->anotaciones ?>
                </div>
                <div class="panel-heading" style="font-size: 1.4em">
                    Tratamientos
                </div>
                <div class="panel-body">
                    <?php echo $datosFicha[0]->tratamientos ?>
                </div>
            </div>
        </div>
    </div>
</div>
<br>

<?php $this->load->view('includes/subbody.php');?>
