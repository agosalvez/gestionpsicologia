<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<?php $this->load->view('includes/topbody.php');?>
<table id="usuarios" class="table table-striped table-bordered" cellspacing="0" width="100%">
    <thead>
    <tr style="cursor: pointer">
        <th>Nombre</th>
        <th>Email</th>
    </tr>
    </thead>
    <tbody>
    <?php if(isset($usuarios)):
        foreach($usuarios as $f): ?>
            <tr>
                <td><?php echo $f->nombre;?></td>
                <td><?php echo $f->email;?></td>
            </tr>
        <?php endforeach;
    endif;?>
    </tbody>
    <tfoot>
    <tr style="cursor: pointer">
        <th>Nombre</th>
        <th>Email</th>
    </tr>
    </tfoot>
</table>
<script>
    $(document).ready(function() {
        $('#usuarios').dataTable( {
            "order": [[ 1, "desc" ]]
        } );
    } );
</script>
<?php $this->load->view('includes/subbody.php');?>

